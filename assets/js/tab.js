// Initiate - Outcomes
const tabsOutcomes = document.querySelectorAll(".tab-outcomes");
const tabBtnsOutcomes = document.querySelectorAll(".tabbtn-outcomes");

// each tab button will have a  click event  listener
tabBtnsOutcomes.forEach((tabBtn) => {
  tabBtn.addEventListener("click", () => {
    // hiding all tabs
    tabsOutcomes.forEach((tab) => {
      tab.classList.add("hidden");
    });

    // showing only the tab that this tab button should show
    const tab = tabBtn.getAttribute("data-tabOutcomes");
    document.getElementById(tab).classList.remove("hidden");

    //  we will give same styles to all tab
    tabBtnsOutcomes.forEach((tabBtn) => {
      tabBtn.className =
        "tabbtn cursor-pointer bg-gray-300 py-3 pb-5 px-5 text-dark-gray font-light rounded-t-lg fluid-content-title-secondary";
    });

    //  we will give special styles to the active tab
    tabBtn.className =
      "tabbtn cursor-pointer bg-outcomes py-3 pb-5 px-5 text-white font-light rounded-t-lg fluid-content-title-secondary";
  });
});

// Initiate - Behave
const tabsBehave = document.querySelectorAll(".tab-behave");
const tabBtnsBehave = document.querySelectorAll(".tabbtn-behave");

// each tab button will have a  click event  listener
tabBtnsBehave.forEach((tabBtn) => {
  tabBtn.addEventListener("click", () => {
    // hiding all tabs
    tabsBehave.forEach((tab) => {
      tab.classList.add("hidden");
    });

    // showing only the tab that this tab button should show
    const tab = tabBtn.getAttribute("data-tabBehave");
    document.getElementById(tab).classList.remove("hidden");

    //  we will give same styles to all tab
    tabBtnsBehave.forEach((tabBtn) => {
      tabBtn.className =
        "tabbtn cursor-pointer bg-gray-300 py-3 pb-5 px-5 text-dark-gray font-light rounded-t-lg fluid-content-title-secondary";
    });

    //  we will give special styles to the active tab
    tabBtn.className =
      "tabbtn cursor-pointer bg-behave py-3 pb-5 px-5 text-white font-light rounded-t-lg fluid-content-title-secondary";
  });
});

// Initiate - Work
const tabsWork = document.querySelectorAll(".tab-work");
const tabBtnsWork = document.querySelectorAll(".tabbtn-work");

// each tab button will have a  click event  listener
tabBtnsWork.forEach((tabBtn) => {
  tabBtn.addEventListener("click", () => {
    // hiding all tabs
    tabsWork.forEach((tab) => {
      tab.classList.add("hidden");
    });

    // showing only the tab that this tab button should show
    const tab = tabBtn.getAttribute("data-tabWork");
    document.getElementById(tab).classList.remove("hidden");

    //  we will give same styles to all tab
    tabBtnsWork.forEach((tabBtn) => {
      tabBtn.className =
        "tabbtn cursor-pointer bg-gray-300 py-3 pb-5 px-5 text-dark-gray font-light rounded-t-lg fluid-content-title-secondary";
    });

    //  we will give special styles to the active tab
    tabBtn.className =
      "tabbtn cursor-pointer bg-work py-3 pb-5 px-5 text-white font-light rounded-t-lg fluid-content-title-secondary";
  });
});

// Initiate - Organise
const tabsOrganise = document.querySelectorAll(".tab-organise");
const tabBtnsOrganise = document.querySelectorAll(".tabbtn-organise");

// each tab button will have a  click event  listener
tabBtnsOrganise.forEach((tabBtn) => {
  tabBtn.addEventListener("click", () => {
    // hiding all tabs
    tabsOrganise.forEach((tab) => {
      tab.classList.add("hidden");
    });

    // showing only the tab that this tab button should show
    const tab = tabBtn.getAttribute("data-tabOrganise");
    document.getElementById(tab).classList.remove("hidden");

    //  we will give same styles to all tab
    tabBtnsOrganise.forEach((tabBtn) => {
      tabBtn.className =
        "tabbtn cursor-pointer bg-gray-300 py-3 pb-5 px-5 text-dark-gray font-light rounded-t-lg fluid-content-title-secondary";
    });

    //  we will give special styles to the active tab
    tabBtn.className =
      "tabbtn cursor-pointer bg-organise py-3 pb-5 px-5 text-white font-light rounded-t-lg fluid-content-title-secondary";
  });
});

// Initiate - Create Value
const tabsCreate = document.querySelectorAll(".tab-create");
const tabBtnsCreate = document.querySelectorAll(".tabbtn-create");

// each tab button will have a  click event  listener
tabBtnsCreate.forEach((tabBtn) => {
  tabBtn.addEventListener("click", () => {
    // hiding all tabs
    tabsCreate.forEach((tab) => {
      tab.classList.add("hidden");
    });

    // showing only the tab that this tab button should show
    const tab = tabBtn.getAttribute("data-tabCreate");
    document.getElementById(tab).classList.remove("hidden");

    //  we will give same styles to all tab
    tabBtnsCreate.forEach((tabBtn) => {
      tabBtn.className =
        "tabbtn cursor-pointer bg-gray-300 py-3 pb-5 px-5 text-dark-gray font-light rounded-t-lg fluid-content-title-secondary";
    });

    //  we will give special styles to the active tab
    tabBtn.className =
      "tabbtn cursor-pointer bg-create-value py-3 pb-5 px-5 text-white font-light rounded-t-lg fluid-content-title-secondary";
  });
});

// Initiate - Learn
const tabsLearn = document.querySelectorAll(".tab-learn");
const tabBtnsLearn = document.querySelectorAll(".tabbtn-learn");

// each tab button will have a  click event  listener
tabBtnsLearn.forEach((tabBtn) => {
  tabBtn.addEventListener("click", () => {
    // hiding all tabs
    tabsLearn.forEach((tab) => {
      tab.classList.add("hidden");
    });

    // showing only the tab that this tab button should show
    const tab = tabBtn.getAttribute("data-tabLearn");
    document.getElementById(tab).classList.remove("hidden");

    //  we will give same styles to all tab
    tabBtnsLearn.forEach((tabBtn) => {
      tabBtn.className =
        "tabbtn cursor-pointer bg-gray-300 py-3 pb-5 px-5 text-dark-gray font-light rounded-t-lg fluid-content-title-secondary";
    });

    //  we will give special styles to the active tab
    tabBtn.className =
      "tabbtn cursor-pointer bg-learn py-3 pb-5 px-5 text-white font-light rounded-t-lg fluid-content-title-secondary";
  });
});
