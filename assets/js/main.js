tailwind.config = {
  theme: {
    extend: {
      colors: {
        "navbar-gray": "#F4F2F3",
        "dark-gray": "#5B5451",
        "dark-green": "#08898C",
        "light-green": "#00D7BE",
        outcomes: "#687C99",
        behave: "#D3378D",
        work: "#4FBAAF",
        organise: "#81358B",
        "create-value": "#F7A52C",
        learn: "#207DC2",
      },
    },
  },
};

// For showing/hiding home content
const homeWheel = document.querySelectorAll(".home-wheel");
const homeContent = document.querySelectorAll(".home-content");

homeWheel.forEach((item) => {
  item.addEventListener("click", () => {
    // add hidden class to all home content
    homeContent.forEach((item) => {
      item.classList.add("hidden");
    });

    // remove hidden class to show specific content
    const content = document.getElementById(item.getAttribute("data-content"));
    content.classList.remove("hidden");
  });
});

//Make home wheel siblings opacity-20 when not click
$(".home-wheel").click(function () {
  $(this).removeClass("opacity-20").removeClass("opacity-100");
  $(this).siblings().addClass("opacity-20");
});

// Show specific page when click
const seeMore = document.querySelectorAll(".see-more");

seeMore.forEach((item) => {
  item.addEventListener("click", () => {
    // make home page hidden
    const content = document.getElementById("home-section");
    content.classList.add("hidden");

    // make specific page show
    const showContent = document.getElementById(
      item.getAttribute("data-button")
    );
    showContent.classList.remove("hidden");
  });
});

// For showing/hiding initiate content
const initiateWheel = document.querySelectorAll(".initiate-wheel");
const initiateContent = document.querySelectorAll(".initiate-content");

initiateWheel.forEach((item) => {
  item.addEventListener("click", () => {
    // add hidden class to all home content
    initiateContent.forEach((item) => {
      item.classList.add("hidden");
    });

    // remove hidden class to show specific content
    const content = document.getElementById(item.getAttribute("data-content"));
    content.classList.remove("hidden");
  });
});

//Make initiate wheel siblings opacity-20 when not click
$(".initiate-wheel").click(function () {
  $(this).removeClass("opacity-20").removeClass("opacity-100");
  $(this).siblings().addClass("opacity-20");
});

// For showing/hiding initiate content
const conveneWheel = document.querySelectorAll(".convene-wheel");
const conveneContent = document.querySelectorAll(".convene-content");

conveneWheel.forEach((item) => {
  item.addEventListener("click", () => {
    // add hidden class to all home content
    conveneContent.forEach((item) => {
      item.classList.add("hidden");
    });

    // remove hidden class to show specific content
    const content = document.getElementById(item.getAttribute("data-content"));
    content.classList.remove("hidden");
  });
});

//Make initiate wheel siblings opacity-20 when not click

$(".convene-shape").click(function () {
  $(this).removeClass("opacity-20");
  $(".convene-main-shape").removeClass("opacity-100").removeClass("opacity-20");

  $(this).siblings(".convene-shape").addClass("opacity-20");

  $("[data-child='" + $(this).attr("data-parent") + "']")
    .addClass("opacity-100")
    .siblings()
    .addClass("opacity-20");
});
